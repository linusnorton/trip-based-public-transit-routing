# Trip Based Public Transit Routing

Journey planning algorithm as described by [Sascha Witt's paper](https://arxiv.org/pdf/1504.07149v2.pdf).
 
## Test

```
npm install
npm test
```

## License

This software is licensed under [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).

Copyright 2016 Linus Norton.